package com.shuogesha.platform.dao;

import java.util.List;
import java.util.Map;

import com.shuogesha.platform.entity.ScheduleJob;

public interface ScheduleJobDao { 
	
	void saveEntity(ScheduleJob bean);

	ScheduleJob findById(Long id);

	void updateById(ScheduleJob bean);
 
	void removeById(Long id);
	
	List<ScheduleJob> queryList(Map<String, Object> map);

	long count(Map<String, Object> map);

	List<ScheduleJob> getList();
}